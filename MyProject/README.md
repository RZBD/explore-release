# Project Summary
Explore is a program based on the Unity game engine which allows you to closely study the planets of our Solar System! 
It is intended for educational use and, in the future, will provide interesting facts about every Planet!

# Requirements

- Any 64 bit version of Windows with DX 11 support
- A 720p 16:9 Display
- Dotnet (.NET) 4.0 or over

# TODO (and what next)

The project is ```NOT``` in a release state! The provided build is the latest prototype! We do not guarantee stability, but would like to receive constructive feedback on errors we are not aware of! Our team is working hard to fix current issues and also polish the project to become a piece of software used by both home users and professionals!

We would like to offer some sneak peaks into our future releases, we're glad to say that our team is working hard to get the project out there! https://imgur.com/a/Hd5TTGC
