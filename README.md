##
<br />
<p align="center">
  <a href="https://www.unrealengine.com">
    <img src="https://i.imgur.com/eFmHQdI.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Explore 3D Model Solar System</h3>

  <p align="center">
     The Unreal Engine based Solar System Explorer
    <br />
    <br />
  </p>
</p>


<p align="center">
	<img src=https://i.imgur.com/ykxsUsn.png width="100%" height="100%">
	</p>

<p align="center">
    	<img src="https://img.shields.io/badge/platform-windows%20%7C%20ios-yellow?style=flat-square">
	<img src="https://img.shields.io/badge/technology-desktop%20%7C%20vr-informational?style=flat-square">
	<img src="https://img.shields.io/badge/size-11.8%20GB-orange?style=flat-square">
	<img src="https://img.shields.io/badge/version-2.1-blue?style=flat-square">
	<img src="https://img.shields.io/badge/UE4-4.20-blueviolet?style=flat-square">
	<img src="https://img.shields.io/badge/Photoshop-CC-blue?style=flat-square">
	<img src="https://img.shields.io/badge/Adobe%20Xd-18.2-ca2c92?style=flat-square">
	<img src="https://img.shields.io/badge/Cinema%204D-R20-blue?style=flat-square">
  </a>
</p>

<br />

## Scopul proiectului

Explore a fost realizat cu scopul de a demonstra cum poate fi îmbunătățită educația prin a deveni o experiență interactivă și captivantă. Acest nou stil de educație a fost exemplificat prin Sistemul Solar care, parte din bazele geografiei.

---

## Cerintele Dezvoltatorilor

În dezvoltarea proiectului, au fost stabilite unele cerințe de baza, ca proiectul să fie accesibil în instituții de învățământ:

* De a putea fi rulat și pe sisteme cu performanță slabă
* De a putea fi rulat pe o interfața Touchscreen
* De a avea informații clare si precise

Multă atenție a fost pusă în informații cât și în modul în care sunt prezentate, deoarece sunt multe dovezi care leagă calitatea prezentării de cum este recepționată informația de către utilizator.

---

## Generalizare

### 1. Progame folosite:


- Unreal Engine 4 - la baza acestei platforme se află limbajul de programare C++, care pentru o utilizare mai ușoară este transpus în Blueprints, care folosește noduri și rețele în loc de linii interminabile de cod.
- Cinema 4D
- Adobe Photoshop CC
- Adobe Experience Design

##

### 2. Specificatii necesare (recomandate):


- Monitor cu Aspect Ratio 16:9
- .NET (DOTNET) 4.7
- Placa Video cu Support DirectX 11
- 2 GB de Stocare

##

### 3. Realizari ale proiectului:

- Development – Unreal Engine 4
- Dezvoltari in Sistemul Educational
- Virtual Reality / Augmented Reality (Coming soon)


<br />


<p align="center">
	<img src=https://i.imgur.com/GIb4Wmq.png width="50%" height="50%">
	</p>



---

## Simularea

Mișcarea planetelor față de Soare este reprezentată la o viteză între 10000 și 30000 de ori mai mare decât viteză reală (viteză poate fi controlată de utilizator) și reprezintă viteză reală a planetelor la scară.

Planetele urmăresc că reper rotația altui obiect invizibil, situat în centrul soarelui, această tehnică fiind mai precisă și mai ușor de realizat.

Utilizatorul poate să se plimbe prin sistemul solar folosind tastele W, A, S, D și să schimbe unghiul de rotație folosind Mouse-ul în timp ce ține Click Dreapta (RClick).
Interfață oferă utilizatorului abilitatea de a schimbă viteză simulării, de a opri simularea și de a vedea în ce an de pe Pământ ar fi.

<br />

<p align="center">
	<img src=https://i.imgur.com/02k1OIK.png width="100%" height="100%">
	</p>

<p align="center">
	<img src=https://i.imgur.com/izDMwGM.png width="100%" height="100%">
	</p>
	
---

## Interfata (User Interface + User Experience)

Interfață întregului proiect a fost lucrată în Adobe Xd împreună cu Adobe Photoshop CC. Scopul acesteia a fost de a putea fi ușor de înțeles pentru utilizator dar în aceelasi timp, de a rămâne în ton cu tema proiectului.

<br />

<p align="center">
	<img src=https://i.imgur.com/bnb325J.png width="100%" height="100%">
	</p>

---

## Informatiile Oferite (sursa nasa.gov)

Informațiile sunt dispuse pentru fiecare planetă în parte, în pagină unică, sursă principală fiind [solarsystem.nasa.gov](https://solarsystem.nasa.gov/)

---

## Texturile Planetelor

Texturile pentru fiecare planetă au fost preluate din [solarsystemscope.com/textures](https://www.solarsystemscope.com/textures/), au fost prelucrate în Adobe Photoshop CC până la nivelul la care acestea au devenit texturi utilizabile în Unreal Engine Material Editor.  Fiecare imagine are o rezoluție de 8K.

<br />

<p align="center">
	<img src=https://i.imgur.com/rAjEsBl.png width="100%" height="100%">
	</p>

---

## Paginile Planetelor

În momentul în care utilizatorul execută click pe obiecte 3D în spațiul vizibil, se activează un eveniment, o variabilă, care are propietatea de ascultător de evenimente (acest eveniment se execută prin schimbarea camerei prin care utilizatorul vede și se inițiază o interfață nouă). Această funcție poate să activeze un eveniment, o variabilă care are proprietatea de “ascultător de evenimente” ce poate fi folosită pentru a execută diverse comenzi.

În cazul de față, acest eveniment rezultă în schimbarea camerei prin care vede utilizatorul și de a iniția interfață nouă.

Pentru a se roti planetele, este detectată mișcarea Mouse-ului indiferent de planeta activă, dar folosește un număr care să determine cărei planete să-i fie schimbat unghiul de rotație. Acest număr este schimbat în funcție de planeta active în momentul în care acel eveniment este activat!

<br />

<p align="center">
	<img src=https://i.imgur.com/x8OySIZ.png width="100%" height="100%">
	</p>

---

## Pagina Evaluarea Cunostintelor

Cu ajutorul aplicației Explore pot fi evaluate cunoștințele utilizatorilor după ce au studiate toate planetele. Testele sunt realizate din informațiile prezentate în descrierea planetelor, întrebările sunt alese aleatoriu dintr-o baza de date cu întrebări referitoare la informațiile din aplicație.

După finalizarea testului, utilizatorul primește un punctaj de la 0 la 10, în funcție de numărul de răspunsuri corecte. De asemenea la sfârșitul testului, utilizatorul are opțiunea de a vedea răspunsurile corecte aferente fiecărui item la care a răspuns greșit.

<br />

<p align="center">
	<img src=https://i.imgur.com/aPqveWF.png width="100%" height="100%">
	</p>

<p align="center">
	<img src=https://i.imgur.com/8BDnkbA.jpg width="100%" height="100%">
	</p>

---

## Virtual Reality + Cross Platforming

Pentru a face proiectul Explore cât mai accesibil, acesta a primit versiuni separate pentru OSX și Linux care fac proiectul accesibil pe cât mai multe configurații.

Pentru a exstinde metodele de input (în proiectul original având o interfață touchscreen cât și pentru mouse și tastatură), Explore are o versiune separată pentru realitatea virtuală.

Interacțiunea cu elementele cosmosului se va face prin folosirea controllerelor. Utilizatorul poate folosi butonul TRIGGER și de a mișcă controllerul pentru a schimbă viteză simulării. Acesta poate, de asemenea, să își schimbe locația folosind butonul joystick și a se uită în direcția în care vrea să ajungă. Un indicator vizual al destinației va fi, de asemenea, oferit. 

<br />

<p align="center">
	<img src=https://i.imgur.com/6nrhWqw.png width="100%" height="100%">
	</p>

<p align="center">
	<img src=https://i.imgur.com/8tL9GGq.png width="100%" height="100%">
	</p>

---

## License

![License](https://img.shields.io/badge/license-Explore-blue?style=flat-square)

- Copyright 2020 © <a href="https://twitter.com/Oreoezi" target="_blank">Codrut Badea</a> & <a href="https://twitter.com/imRazvanBadea" target="_blank">Razvan Badea</a>.
- Note: Pozele nu au fost facute in versiunea finala a proiectului.
